---
to: src/day<%= index %>/__test__/puzzle02.test.js
---
const solve = require('../puzzle02')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual('solution')
})