---
to: src/day<%= index %>/puzzle02.js
---
const parse = require('./parse')

const solve = inputPath => {
  const parsed = parse(inputPath)
  return 'solution'
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


