---
to: src/day<%= index %>/parse.js
---
const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    return line
  })

  return parsed
}

module.exports = parse
