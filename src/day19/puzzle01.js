const parse = require('./parse')

const dynamicPossibilities = possibilitiesPerPosition => {
  if (possibilitiesPerPosition.length === 1) {
    return possibilitiesPerPosition[0]
  } else {
    const possibilities = possibilitiesPerPosition[0].map(firstPositionPossibility => {
      return dynamicPossibilities(possibilitiesPerPosition.slice(1)).map(nextPositionsPosibility => {
        return firstPositionPossibility.concat(nextPositionsPosibility)
      })
    })
    return [].concat(...possibilities)
  }
}

const solve = inputPath => {
  const { rules, messages } = parse(inputPath)
  const memo = new Map()
  const rulePossibilities = ruleIndex => {
    if (memo.has(ruleIndex)) {
      return memo.get(ruleIndex)
    }
    const rule = rules.get(ruleIndex)
    let possibilities = null
    if (rule.type === 'primary') {
      possibilities = [rule.word]
    } else if (rule.type === 'secondary') {
      possibilities = [].concat(...rule.subrules.map(indexes => {
        const possibilitiesPerIndex = indexes.map(index => {
          return rulePossibilities(index)
        })
        return dynamicPossibilities(possibilitiesPerIndex)
      }))
    } else {
      throw new Error(`Rule type ${rule.type} is not handled`)
    }
    memo.set(ruleIndex, possibilities)
    return possibilities
  }

  return messages
    .filter(message => {
      const possibilities = rulePossibilities(0)
      return possibilities.includes(message)
    })
    .length
}

module.exports = { solve, dynamicPossibilities }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


