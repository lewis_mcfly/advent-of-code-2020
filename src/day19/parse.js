const { readlines, chunkify } = require('../utils')

const parse = inputPath => {
  const [rules, messages] = chunkify(readlines(inputPath, false), '')
  const rulesMap = new Map()
  rules.forEach(rule => {
    const { groups: { index } } = /^(?<index>\d*):/.exec(rule)
    const primaryRuleRegex = /^\d*: "(?<word>\w*)"$/
    const secondaryRuleRegex = /^\d*: (?<subrules>((\d*\s?)*\|?)*)$/
    if (primaryRuleRegex.test(rule)) {
      const { groups: { word } } = primaryRuleRegex.exec(rule)
      rulesMap.set(parseInt(index), { 
        type: 'primary', 
        word 
      })
    } else if (secondaryRuleRegex.test(rule)) {
      let { groups: { subrules } } = secondaryRuleRegex.exec(rule)
      rulesMap.set(parseInt(index), { 
        type: 'secondary', 
        subrules: subrules.split('|').map(subrule => subrule.trim().split(' ').map(index => parseInt(index))) 
      })
    } else {
      throw new Error('An error occured while parsing the input file rules')
    }
  })

  return { rules: rulesMap, messages }
}

module.exports = parse
