const parse = require('./parse')

const solve = inputPath => {
  let { rules, messages } = parse(inputPath)

  const regexes = new Map()
  const ruleRegex = ruleIndex => {
    if (regexes.has(ruleIndex)) {
      return regexes.get(ruleIndex)
    }
    const rule = rules.get(ruleIndex)
    let regex = ''
    if (rule.type === 'primary') {
      regex = rule.word
    } else if (rule.type === 'secondary') {
      regex = rule.subrules
        .map(indexes => indexes
          .map(index => ruleRegex(index))
          .join(''))
        .join('|')
      regex = `(${regex})`
    } else {
      throw new Error(`Rule type ${rule.type} is not handled`)
    }
    return regex
  }

  for (let index of rules.keys()) {
    if (!([0, 8, 11].includes(index))) {
      regexes.set(index, ruleRegex(index))
    }
  }

  const fortySecondRegex = new RegExp(`^(${regexes.get(42)})$`)
  const thirtyFirstRegex = new RegExp(`^(${regexes.get(31)})$`)
  const eleventhRegex = new RegExp(`^(?<left>${regexes.get(42)})(?<middle>.*)(?<right>${regexes.get(31)})$`)
  const isValidToEleven = string => {
    if (!eleventhRegex.test(string)) {
      return false
    } else {
      for (let i = 1; i <= (string.length / 2); i += 1) {
        const left = string.slice(0, i)
        const right = string.slice(-i)
        if (fortySecondRegex.test(left) && thirtyFirstRegex.test(right)) {
          const middle = string.slice(i, -i)
          if (middle.length === 0 || isValidToEleven(middle)) {
            return true
          }
        }
      }
    }
  }
  
  const zeroRegex = new RegExp(`^(?<left>${regexes.get(42)}+)(?<right>.+)$`)
  const eighthRegex = new RegExp(`^${regexes.get(42)}+$`)
  const isValidToZero = string => {
    if (!zeroRegex.test(string)) {
      return false
    } else {
      for (let i = 1; i <= string.length; i += 1) {
        const left = string.slice(0, i)
        if (eighthRegex.test(left)) {
          const right = string.slice(i)
          if (isValidToEleven(right)) {
            return true
          }
        }
      }
      return false
    }
  }

  const validMessages = messages.filter(message => isValidToZero(message))
  return validMessages.length
}

module.exports = { solve }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
