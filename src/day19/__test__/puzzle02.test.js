const { solve } = require('../puzzle02')

test('solves test input 2', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(12)
})

test('solves test input 4', () => {
  const solution = solve(`${__dirname}/input-04.txt`)
  expect(solution).toEqual(2)
})
