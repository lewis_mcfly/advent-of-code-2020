const { solve, dynamicPossibilities } = require('../puzzle01')

test('find possibilities dynamically 1', () => {
  expect(dynamicPossibilities([['a', 'b'], ['0', '1']])).toEqual(expect.arrayContaining(['a0', 'a1', 'b0', 'b1']))
})

test('find possibilities dynamically 2', () => {
  expect(dynamicPossibilities([['a', 'b'], ['x', 'y', 'z'], ['0']])).toEqual(expect.arrayContaining(
    ['ax0','ay0', 'az0', 'bx0', 'by0', 'bz0']
  ))
})

test('solves test input 1', () => {
  const solution = solve(`${__dirname}/input-01.txt`)
  expect(solution).toEqual(2)
})

test('solves test input 2', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(3)
})
