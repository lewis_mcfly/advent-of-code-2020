const parse = require('../parse')

test('parses test input', () => {
  const parsed = parse(`${__dirname}/input-01.txt`)
  expect([...parsed.rules]).toEqual(expect.arrayContaining([
    [0, { type: 'secondary', subrules: [[4, 1, 5]] }],
    [1, { type: 'secondary', subrules: [[2, 3], [3, 2]] }],
    [2, { type: 'secondary', subrules: [[4, 4], [5, 5]] }],
    [3, { type: 'secondary', subrules: [[4, 5], [5, 4]] }],
    [4, { type: 'primary', word: 'a' }],
    [5, { type: 'primary', word: 'b' }],
  ]))
  expect(parsed.messages).toEqual([
    'ababbb',
    'bababa',
    'abbbab',
    'aaabbb',
    'aaaabbb',
  ])
})