const { solve } = require('./puzzle01')
const { every } = require('../utils')

const viewFrom = (seats, x, y) => {
  const directions = [
    { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: 0 }, { x: 1, y: -1 },
    { x: 0, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 0 }, { x: -1, y: 1 }
  ]
  
  let viewedSeats = directions.map(direction => {
    let y2 = y + direction.y
    let x2 = x + direction.x
    let viewedSeat = null
    while (y2 >= 0 && y2 < seats.length && x2 >= 0 && x2 < seats[y2].length) {
      const seat = seats[y2][x2]
      if (seat !== '.') {
        viewedSeat = seat
        break
      }
      x2 += direction.x
      y2 += direction.y
    }
    return viewedSeat
  })

  return viewedSeats.filter(seat => seat !== null)
}

const play = seats => {  
  let stayedStill = true
  const clone = seats.map(line => [...line])
  for (let y = 0; y < seats.length; y += 1) {
    for (let x = 0; x < seats[y].length; x += 1) {
      if (seats[y][x] !== '.') {
        const viewedSeats = viewFrom(seats, x, y)
        if (seats[y][x] === 'L' && every(viewedSeats, seat => seat !== '#')){
          clone[y][x] = '#'
          stayedStill = false
        } else if (seats[y][x] === '#' && viewedSeats.filter(seat => seat === '#').length >= 5) {
          clone[y][x] = 'L'
          stayedStill = false
        }
      }
    }
  }
  return { seats: clone, stayedStill }
}

module.exports = { play, viewFrom }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, play)
  console.log(`The solution is ${solution}`)
}
