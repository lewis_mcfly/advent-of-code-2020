const { solve } = require('../puzzle01')
const { play, viewFrom } = require('../puzzle02')
const parse = require('../parse')
const { readlines, chunkify } = require('../../utils')

const seats = parse(`${__dirname}/input.txt`)
const expected = chunkify(readlines(`${__dirname}/input-expected-rounds-02.txt`), '----------').map(seatMatrix => seatMatrix.map(seatline => seatline.split('')))
const rounds = [seats, ...expected]


test('viewFrom 01', () => {
  expect(viewFrom(parse(`${__dirname}/view-01.txt`), 3, 4)).toEqual(['#', '#', '#', '#', '#', '#', '#', '#'])
})

test('viewFrom 02', () => {
  expect(viewFrom(parse(`${__dirname}/view-02.txt`), 1, 1)).toEqual(['L'])
})

test('viewFrom 03', () => {
  expect(viewFrom(parse(`${__dirname}/view-03.txt`), 3, 3)).toEqual([])
})

test('play round 0 correctly', () => {
  expect(play(rounds[0]).seats).toEqual(rounds[1])
})

test('play round 1 correctly', () => {
  expect(play(rounds[1]).seats).toEqual(rounds[2])
})

test('play round 2 correctly', () => {
  expect(play(rounds[2]).seats).toEqual(rounds[3])
})

test('play round 3 correctly', () => {
  expect(play(rounds[3]).seats).toEqual(rounds[4])
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`, play)
  expect(solution).toEqual(26)
})