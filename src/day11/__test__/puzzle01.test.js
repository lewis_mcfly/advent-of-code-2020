const { solve, play } = require('../puzzle01')
const parse = require('../parse')
const { readlines, chunkify } = require('../../utils')

const seats = parse(`${__dirname}/input.txt`)
const expected = chunkify(readlines(`${__dirname}/input-expected-rounds-01.txt`), '----------').map(seatMatrix => seatMatrix.map(seatline => seatline.split('')))
const rounds = [seats, ...expected]

test('play round 0 correctly', () => {
  expect(play(rounds[0]).seats).toEqual(rounds[1])
})

test('play round 1 correctly', () => {
  expect(play(rounds[1]).seats).toEqual(rounds[2])
})

test('play round 2 correctly', () => {
  expect(play(rounds[2]).seats).toEqual(rounds[3])
})

test('play round 3 correctly', () => {
  expect(play(rounds[3]).seats).toEqual(rounds[4])
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`, play)
  expect(solution).toEqual(37)
})