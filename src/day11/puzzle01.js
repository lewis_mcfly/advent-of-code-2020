const parse = require('./parse')
const { every } = require('../utils')

const play = seats => {
  const getAdjacentSeats = (x, y) => {
    const adjacentSeats = []
    for (let j = y - 1; j <= y + 1; j += 1) {
      for (let i = x - 1; i <= x + 1; i += 1) {
        if (!(i === x && j === y)) {
          if (j >= 0 && j < seats.length && i >= 0 && i < seats[j].length) {
            adjacentSeats.push(seats[j][i])
          }
        }
      }
    }
    return adjacentSeats
  }
  
  let stayedStill = true
  const clone = seats.map(line => [...line])
  for (let y = 0; y < seats.length; y += 1) {
    for (let x = 0; x < seats[y].length; x += 1) {
      const adjacentSeats = getAdjacentSeats(x, y)
      if (seats[y][x] === 'L' && every(adjacentSeats, seat => seat !== '#')){
        clone[y][x] = '#'
        stayedStill = false
      } else if (seats[y][x] === '#' && adjacentSeats.filter(seat => seat === '#').length >= 4) {
        clone[y][x] = 'L'
        stayedStill = false
      }
    }
  }
  return { seats: clone, stayedStill }
}

const solve = (inputPath, play) => {
  let state = { seats: parse(inputPath), stayedStill: false }
  while (!state.stayedStill) {
    state = play(state.seats)
  }

  return state.seats
    .map(seatline => seatline.reduce((acc, value) => value === '#' ? acc + 1 : acc , 0))
    .reduce((a, b) => a + b)
}

module.exports = { solve, play }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, play)
  console.log(`The solution is ${solution}`)
}


