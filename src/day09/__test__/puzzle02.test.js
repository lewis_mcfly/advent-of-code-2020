const solve = require('../puzzle02')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`, 5)
  expect(solution).toEqual(62)
})