const parse = require('./parse')
const { findInvalidNumber }  = require('./puzzle01')
const { sum, min, max } = require('../utils')

const solve = (inputPath, preambleSize) => {
  const series = parse(inputPath)
  const invalidNumber = findInvalidNumber(series, preambleSize)
  const contiguousSet = (() => {
    for (let n = 2; n <= series.length; n += 1) {
      let currentSum = sum(series.slice(0, n))
      for (let i = 1; i < series.length - n; i += 1) {
        if (currentSum === invalidNumber) {
          return series.slice(i - 1, i + n - 1)
        }
        currentSum += series[i + n - 1] - series[i - 1]
      }
    }
    throw new Error('Contigious set not found')
  })()
  return min(contiguousSet) + max(contiguousSet)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, 25)
  console.log(`The solution is ${solution}`)
}
