const parse = require('./parse')

const findInvalidNumber = (series, preambleSize) =>  {
  const preamble = series.slice(0, preambleSize)
  const validateNumber = n => {
    for (const a of preamble) {
      for (const b of preamble) {
        if (a !== b && a + b === n) {
          return true
        }
      } 
    }
    return false
  }

  for (let i = preambleSize; i < series.length; i += 1) {
    const n = series[i]
    if (!validateNumber(n)) {
      return n
    }
    preamble.shift()
    preamble.push(n)
  }

  throw new Error('No invalid number was found')
}

const solve = (inputPath, preambleSize) => findInvalidNumber(parse(inputPath), preambleSize)

module.exports = { solve, findInvalidNumber }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, 25)
  console.log(`The solution is ${solution}`)
}
