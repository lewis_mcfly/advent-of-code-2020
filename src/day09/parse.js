const { readlines } = require('../utils')

const parse = inputPath => readlines(inputPath).map(line => parseInt(line))

module.exports = parse
