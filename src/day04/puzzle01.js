const { every } = require('../utils')
const parse = require('./parse')

const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
const validate = passport => every(requiredFields, field => field in passport)

const solve = inputPath => {
  const passports = parse(inputPath)
  const validPassports = passports.filter(passport => validate(passport))
  return validPassports.length
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


