const { every } = require('../utils')
const parse = require('./parse')

const requiredFields = [
  { field: 'byr', validate: value => value.match(/^\d{4}$/) && parseInt(value) >= 1920 && parseInt(value) <= 2002 },
  { field: 'iyr', validate: value => value.match(/^\d{4}$/) && parseInt(value) >= 2010 && parseInt(value) <= 2020 },
  { field: 'eyr', validate: value => value.match(/^\d{4}$/) && parseInt(value) >= 2020 && parseInt(value) <= 2030 },
  { field: 'hgt', validate: value => {
    const result = /^(?<size>\d*)(?<unit>(in|cm))$/.exec(value)
    if (!result) return false
    const { groups: { unit, size } } = result
    return unit === 'in' ? (parseInt(size) >= 59 && parseInt(size) <= 76) : (parseInt(size) >= 150 && parseInt(size) <= 193)
  }},
  { field: 'hcl', validate: value => value.match(/^#(\d|[abcdef]){6}$/) },
  { field: 'ecl', validate: value => ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(value) },
  { field: 'pid', validate: value => value.match(/^\d{9}$/) },
]

const validate = passport => every(requiredFields, requiredField => requiredField.field in passport && requiredField.validate(passport[requiredField.field]))
const solve = inputPath => {
  const passports = parse(inputPath)
  const validPassports = passports.filter(passport => validate(passport))
  return validPassports.length
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
