const solve = require('../puzzle02')

test('solves input with valid passports', () => {
  const solution = solve(`${__dirname}/validPassports.txt`)
  expect(solution).toEqual(4)
})

test('solves input with invalid passports', () => {
  const solution = solve(`${__dirname}/invalidPassports.txt`)
  expect(solution).toEqual(0)
})