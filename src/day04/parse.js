const { readlines } = require('../utils')

const parse = inputPath => {
  let passports = []
  let passport = {}
  for (const line of readlines(inputPath, false)) {
    if (line.length > 0) {
      const fields = line
        .split(' ')
        .map(field => field.split(':'))
        .reduce((a, b) => ({ ...a, [b[0]]: b[1] }), {})
      passport = { ...passport, ...fields }
    } else {
      passports.push({ ...passport })
      passport = {}
    }
  }

  return passports
}

module.exports = parse
