const solve = require('../puzzle02')

test('solves test input', () => {
  const validPasswordsCount = solve(`${__dirname}/input.txt`)
  expect(validPasswordsCount).toEqual(1)
})