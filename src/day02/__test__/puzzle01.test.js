const solve = require('../puzzle01')

test('solves test input', () => {
  const validPasswordsCount = solve(`${__dirname}/input.txt`)
  expect(validPasswordsCount).toEqual(2)
})