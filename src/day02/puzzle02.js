const parse = require('./parse')

const xor = (a, b) => (a || b) && !(a && b)

const solve = inputPath => {
  const passwords = parse(inputPath).map(({ min, max, ...parsed }) => ({ firstPosition: min, secondPosition: max, ...parsed }))

  const validPasswords = passwords.filter(({ firstPosition, secondPosition, selectiveCharacter, password }) => {
    const characters = password.split('')
    return xor(characters[firstPosition - 1] == selectiveCharacter, characters[secondPosition - 1] == selectiveCharacter)
  })

  return validPasswords.length
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
