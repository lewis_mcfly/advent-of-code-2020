const parse = require('./parse')

const solve = inputPath => {
  const passwords = parse(inputPath)

  const validPasswords = passwords.filter(({ min, max, selectiveCharacter, password }) => {
    const occurences = password.split('').filter(character => character === selectiveCharacter).length
    return occurences >= min && occurences <= max
  })

  return validPasswords.length
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
