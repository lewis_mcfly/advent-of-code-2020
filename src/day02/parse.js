const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    const { groups: { min, max, selectiveCharacter, password } } = /(?<min>\d*)-(?<max>\d*) (?<selectiveCharacter>.): (?<password>.*)/.exec(line)
    return { min: parseInt(min), max: parseInt(max), selectiveCharacter, password }
  })

  return parsed
}

module.exports = parse
