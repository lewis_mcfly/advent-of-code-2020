const parse = require('./parse')
const { readSeat } = require('./puzzle01')

const solve = inputPath => {
  const seats = parse(inputPath)
    .map(description => readSeat(description))
    .sort((a, b) => a.id > b.id ? 1 : -1)

  for (const [index, seat] of seats.entries()) {
    if (index > 0 && seat.id > seats[index - 1].id + 1) {
      return { id: seat.id - 1, row: Math.floor((seat.id - 1) / 8), column: (seat.id - 1) % 8 }
    }
  }

  throw new Error('Seat not found')
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is seat ${solution.id} at row ${solution.row} and column ${solution.column}`)
}


