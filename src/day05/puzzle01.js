const parse = require('./parse')

const readSeat = description => {
  let minRow = 0
  let maxRow = 127
  for (const character of description.slice(0, 7)) {
    const gap = Math.ceil((maxRow - minRow) / 2)
    if (character === 'F') {
      maxRow -= gap
    } else {
      minRow += gap
    }
  }

  let minColumn = 0
  let maxColumn = 7
  for (const character of description.slice(7)) {
    const gap = Math.ceil((maxColumn - minColumn) / 2)
    if (character === 'L') {
      maxColumn -= gap
    } else {
      minColumn += gap
    }
  }

  return { row: minRow, column: minColumn, id: minRow * 8 + minColumn }
}

const solve = inputPath => 
  parse(inputPath)
    .map(description => readSeat(description))
    .reduce((a, b) => a.id > b.id ? a : b)
    .id

module.exports = { solve, readSeat }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


