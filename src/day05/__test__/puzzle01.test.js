const { solve, readSeat } = require('../puzzle01')

test('read seat', () => {
  expect(readSeat('FBFBBFFRLR')).toEqual({ row: 44, column: 5, id: 357 })
  expect(readSeat('BFFFBBFRRR')).toEqual({ row: 70, column: 7, id: 567 })
  expect(readSeat('FFFBBBFRRR')).toEqual({ row: 14, column: 7, id: 119 })
  expect(readSeat('BBFFBBFRLL')).toEqual({ row: 102, column: 4, id: 820 })
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(820)
})
