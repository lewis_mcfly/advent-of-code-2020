const { readlines } = require('../utils')

const parse = inputPath => readlines(inputPath)

module.exports = parse
