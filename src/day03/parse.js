const { readlines } = require('../utils')

const parse = inputPath => readlines(inputPath).map(line => line.split(''))

module.exports = parse
