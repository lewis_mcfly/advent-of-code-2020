const solve01 = require('./puzzle01')

const solve = (inputPath, slopes) => 
  slopes
    .map(slope => solve01(inputPath, slope))
    .reduce((a, b) => a * b)

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, [
    { x: 1, y: 1 },
    { x: 3, y: 1 },
    { x: 5, y: 1 },
    { x: 7, y: 1 },
    { x: 1, y: 2 },
  ])
  console.log(`The solution is ${solution}`)
}
