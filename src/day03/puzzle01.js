const parse = require('./parse')

const solve = (inputPath, slope) => {
  let map = parse(inputPath)
  const height = map.length
  const width = map[0].length
  let treeCount = 0

  for (let n = 0; n * slope.y < height; n += 1) {
    const to = map[n * slope.y][(n * slope.x) % width]
    if (to === '#') {
      treeCount += 1
    }
  }

  return treeCount
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, { x: 3, y: 1 })
  console.log(`The solution is ${solution}`)
}


