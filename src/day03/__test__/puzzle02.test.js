const solve01 = require('../puzzle01')
const solve02 = require('../puzzle02')

test('solve01', () => {
  expect(solve01(`${__dirname}/input.txt`, { x: 1, y: 1 })).toEqual(2)
  expect(solve01(`${__dirname}/input.txt`, { x: 3, y: 1 })).toEqual(7)
  expect(solve01(`${__dirname}/input.txt`, { x: 5, y: 1 })).toEqual(3)
  expect(solve01(`${__dirname}/input.txt`, { x: 7, y: 1 })).toEqual(4)
  expect(solve01(`${__dirname}/input.txt`, { x: 1, y: 2 })).toEqual(2)
})

test('solve02', () => {
  expect(solve02(`${__dirname}/input.txt`, [
    { x: 1, y: 1 },
    { x: 3, y: 1 },
    { x: 5, y: 1 },
    { x: 7, y: 1 },
    { x: 1, y: 2 },
  ])).toEqual(336)
})