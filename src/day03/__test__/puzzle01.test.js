const solve = require('../puzzle01')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`, { x: 3, y: 1 })
  expect(solution).toEqual(7)
})