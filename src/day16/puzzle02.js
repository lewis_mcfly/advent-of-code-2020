const { every, exists } = require('../utils')
const parse = require('./parse')

const validateValue = (value, rules) => {
  for (const rule of rules) {
    if (value >= rule.min && value <= rule.max) {
      return true
    }
  }
  return false
}

const getFieldsPositions = (fields, tickets)  => {
  const validTickets = tickets
    .filter(values => {
      return every(values, value => exists(fields, field => validateValue(value, field.rules)))
    })

  let possibleFieldsPerPosition = Array(fields.length).fill([]).map((_, index) => {
    return fields.filter(field => {
      for (const ticket of validTickets) {
        if (!validateValue(ticket[index], field.rules)) {
          return false
        }
      }
      return true
    })
  })

  // eslint-disable-next-line no-constant-condition
  while (true) {
    const uniqueFields = possibleFieldsPerPosition.filter(fields => fields.length === 1).map(fields => fields[0].name)
    if (uniqueFields.length === possibleFieldsPerPosition.length) {
      break
    }
    possibleFieldsPerPosition = possibleFieldsPerPosition.map(possibleFields => {
      return (possibleFields.length > 1 ? possibleFields.filter(field => !uniqueFields.includes(field.name)) : possibleFields)
    })
  }

  return possibleFieldsPerPosition.map(possibleFields => possibleFields[0].name)
}

const solve = inputPath => {
  const { fields, myTicket, nearbyTickets } = parse(inputPath)
  const fieldsPositions = getFieldsPositions(fields, nearbyTickets)
  const departureFieldsIndexes = fieldsPositions
    .map((value, index) => ({ value, index }))
    .filter(({ value }) => value.startsWith('departure'))
    .map(({ index }) => index)
  return departureFieldsIndexes.reduce((acc, index) => acc * myTicket[index], 1)
}

module.exports = { getFieldsPositions, solve }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
