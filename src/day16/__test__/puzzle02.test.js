const parse = require('../parse')
const { getFieldsPositions, solve } = require('../puzzle02')

test('get fields positions', () => {
  const { fields, nearbyTickets } = parse(`${__dirname}/input-02.txt`)
  expect(getFieldsPositions(fields, nearbyTickets)).toEqual(['row', 'departure-class', 'departure-seat'])
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(156)
})
