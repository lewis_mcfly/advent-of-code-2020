const solve = require('../puzzle01')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input-01.txt`)
  expect(solution).toEqual(71)
})