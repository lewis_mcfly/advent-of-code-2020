const { readlines, chunkify, slugify } = require('../utils')

const parse = inputPath => {
  let [fields, myTicket, nearbyTickets] = chunkify(readlines(inputPath, false), '')

  fields = fields.map(line => {
    const { groups: { field, rules } } = /^(?<field>[\w\s-]*): (?<rules>.*)$/.exec(line)
    return { name: slugify(field), rules: rules.split(' or ').map(rule => {
      const { groups: { min, max } } = /^(?<min>\d*)-(?<max>\d*)$/.exec(rule)
      return { min: parseInt(min), max: parseInt(max) }
    })}
  })

  myTicket = myTicket[1].split(',').map(value => parseInt(value))
  nearbyTickets = nearbyTickets.slice(1).map(nearbyTicket => nearbyTicket.split(',').map(value => parseInt(value)))

  return { fields, myTicket, nearbyTickets }
}

module.exports = parse
