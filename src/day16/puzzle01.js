const parse = require('./parse')

const solve = inputPath => {
  const { fields, nearbyTickets } = parse(inputPath)

  const rules = fields
    .map(field => field.rules)
    .reduce((acc, rules) => acc.concat(rules))

  return nearbyTickets
    .reduce((acc, values) => acc.concat(values))
    .filter(value => {
      for (const rule of rules) {
        if (value >= rule.min && value <= rule.max) {
          return false
        }
      }
      return true
    })
    .reduce((a, b) => a + b)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


