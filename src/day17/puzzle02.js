const parse = require('./parse')

const solve = inputPath => {
  const space = parse(inputPath)

  const blankLine = (length) => (new Array(length)).fill('.')
  const blankArea = (length, width) => (new Array(width)).fill(0).map(() => blankLine(length))
  const blankSpace = (length, width, heigth) => (new Array(heigth)).fill(0).map(() => blankArea(length, width))
  const activeNearby = (x, y, z, w) => {
    const points = []
    for (let zi = Math.max(z - 1, 0); zi <= Math.min(z + 1, system.length - 1); zi += 1) {
      for (let yi = Math.max(y - 1, 0); yi <= Math.min(y + 1, system[zi].length - 1); yi += 1) {
        for (let xi = Math.max(x - 1, 0); xi <= Math.min(x + 1, system[zi][yi].length - 1); xi += 1) {
          for (let wi = Math.max(w - 1, 0); wi <= Math.min(w + 1, system[zi][yi][xi].length - 1); wi += 1) {
            if (xi !== x || yi !== y || zi !== z || wi !== w) {
              points.push(system[zi][yi][xi][wi])
            }
          }
        }
      }
    }
    return points.filter(point => point === '#').length
  }

  let system = [
    blankSpace(space[0][0].length, space[0].length, space.length),
    space,
    blankSpace(space[0][0].length, space[0].length, space.length),
  ]

  for (let cycle = 0; cycle < 6; cycle += 1) {
    system = system.map(
      space => [
        blankArea(space[0][0].length + 2, space[0].length + 2),
        ...space.map(area => [
          blankLine(area[0].length + 2),
          ...area.map(line => ['.', ...line, '.']),
          blankLine(area[0].length + 2)
        ]),
        blankArea(space[0][0].length + 2, space[0].length + 2)
      ]
    )
    system.unshift(blankSpace(system[0][0][0].length, system[0][0].length, system[0].length))
    system.push(blankSpace(system[0][0][0].length, system[0][0].length, system[0].length))

    const newSystem = []
    for (let z = 0; z < system.length; z += 1) {
      const space = []
      for (let y = 0; y < system[0].length; y += 1) {
        const area = []
        for (let x = 0; x < system[0][0].length; x += 1) {
          const line = []
          for (let w = 0; w < system[0][0][0].length; w += 1) {
            if (system[z][y][x][w] === '#') {
              line.push([2, 3].includes(activeNearby(x, y, z, w)) ? '#' : '.')
            } else {
              line.push(activeNearby(x, y, z, w) === 3 ? '#' : '.')
            }
          }
          area.push(line)
        }
        space.push(area)
      }
      newSystem.push(space)
    }
    system = newSystem
  }

  let active = 0
  for (let z = 0; z < system.length; z += 1) {
    for (let y = 0; y < system[0].length; y += 1) {
      for (let x = 0; x < system[0][0].length; x += 1) {
        for (let w = 0; w < system[0][0][0].length; w += 1) {
          if (system[z][y][x][w] === '#') {
            active += 1
          }
        }
      }
    }
  }
  return active
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
