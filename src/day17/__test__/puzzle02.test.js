const solve = require('../puzzle02')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input.txt`)
  expect(solution).toEqual(848)
})