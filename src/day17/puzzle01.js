const parse = require('./parse')

const solve = inputPath => {
  let system = parse(inputPath)

  const blankLine = (length) => (new Array(length)).fill('.')
  const blankArea = (length, width) => (new Array(width)).fill(0).map(() => blankLine(length))
  const activeNearby = (x, y, z) => {
    const points = []
    for (let zi = Math.max(z - 1, 0); zi <= Math.min(z + 1, system.length - 1); zi += 1) {
      for (let yi = Math.max(y - 1, 0); yi <= Math.min(y + 1, system[zi].length - 1); yi += 1) {
        for (let xi = Math.max(x - 1, 0); xi <= Math.min(x + 1, system[zi][yi].length - 1); xi += 1) {
          if (xi !== x || yi !== y || zi !== z) {
            points.push(system[zi][yi][xi])
          }
        }
      }
    }
    return points.filter(point => point === '#').length
  }

  for (let cycle = 0; cycle < 6; cycle += 1) {
    system = system.map(area => [blankLine(area[0].length + 2), ...area.map(line => ['.', ...line, '.']), blankLine(area[0].length + 2)])
    system.unshift(blankArea(system[0][0].length, system[0].length))
    system.push(blankArea(system[0][0].length, system[0].length))

    const newSystem = []
    for (let z = 0; z < system.length; z += 1) {
      const area = []
      for (let y = 0; y < system[0].length; y += 1) {
        const line = []
        for (let x = 0; x < system[0][0].length; x += 1) {
          if (system[z][y][x] === '#') {
            line.push([2, 3].includes(activeNearby(x, y, z)) ? '#' : '.')
          } else {
            line.push(activeNearby(x, y, z) === 3 ? '#' : '.')
          }
        }
        area.push(line)
      }
      newSystem.push(area)
    }
    system = newSystem
  }

  let active = 0
  for (let z = 0; z < system.length; z += 1) {
    for (let y = 0; y < system[0].length; y += 1) {
      for (let x = 0; x < system[0][0].length; x += 1) {
        if (system[z][y][x] === '#') {
          active += 1
        }
      }
    }
  }
  return active
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


