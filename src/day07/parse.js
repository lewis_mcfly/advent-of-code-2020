const { readlines, slugify } = require('../utils')

const parse = inputPath => {
  const rules = {}
  for (const line of readlines(inputPath)) {
    let { groups: { containingBagColor, containedBags } } = /^(?<containingBagColor>\w*\s\w*) bags contain (?<containedBags>[\w\s,]*)\.$/.exec(line)
    if (containedBags === 'no other bags') {
      containedBags = null
    } else {
      containedBags = containedBags
        .split(',')
        .map(containedBag => containedBag.trim())
        .map(containedBag => {
          const { groups: { count, color } } = /^(?<count>\d*)\s(?<color>\w*\s\w*)\sbags?$/.exec(containedBag)
          return { count: parseInt(count), color: slugify(color)}
        })
    }
    rules[slugify(containingBagColor)] = containedBags
  }

  return rules
}

module.exports = parse
