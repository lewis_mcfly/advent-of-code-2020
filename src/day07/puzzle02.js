const parse = require('./parse')

const solve = (inputPath, wantedColor) => {
  const rules = parse(inputPath)
  const childrenBagsCount = color => 
    !rules[color] ?
      0 : 
      rules[color].reduce((acc, childBag) => { 
        return acc + childBag.count + childBag.count * childrenBagsCount(childBag.color)
      }, 0)
  return childrenBagsCount(wantedColor)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, 'shiny-gold')
  console.log(`The solution is ${solution}`)
}
