const solve = require('../puzzle02')

test('solves test with input 01', () => {
  const solution = solve(`${__dirname}/input-01.txt`, 'shiny-gold')
  expect(solution).toEqual(32)
})

test('solves test with input 02', () => {
  const solution = solve(`${__dirname}/input-02.txt`, 'shiny-gold')
  expect(solution).toEqual(126)
})