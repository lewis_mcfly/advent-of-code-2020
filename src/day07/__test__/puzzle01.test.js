const solve = require('../puzzle01')

test('solves test input', () => {
  const solution = solve(`${__dirname}/input-01.txt`, 'shiny-gold')
  expect(solution).toEqual(4)
})