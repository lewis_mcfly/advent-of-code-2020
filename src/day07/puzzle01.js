const parse = require('./parse')

const solve = (inputPath, wantedColor) => {
  const rules = parse(inputPath)
  const bagsAbleToContainColor = color => {
    const parentBagColors = Object.keys(rules).filter(containingBagColor => {
      if (rules[containingBagColor] !== null) {
        for (const containedBag of rules[containingBagColor]) {
          if (containedBag.color === color && containedBag.count > 0) {
            return true
          }
        }
      }
      return false
    })

    let result = [...parentBagColors]
    for (const parentBagColor of parentBagColors) {
      result = result.concat(
        bagsAbleToContainColor(parentBagColor).filter(grandParentColor => !result.includes(grandParentColor))
      )
    }
    return result
  }

  return bagsAbleToContainColor(wantedColor).length
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`, 'shiny-gold')
  console.log(`The solution is ${solution}`)
}
