const { solve, matchingBinaries, applyMask } = require('../puzzle02')

const str2bin = str => str.split('').map(bit => bit === 'X' ? bit : parseInt(bit))
const bin2str = bin => bin.join('')

test('matching binaries', () => {
  expect(matchingBinaries(str2bin('000000000000000000000000000000X1001X')).map(bin => bin2str(bin)))
    .toEqual(expect.arrayContaining([
      '000000000000000000000000000000010010',
      '000000000000000000000000000000010011',
      '000000000000000000000000000000110010',
      '000000000000000000000000000000110011'
    ]))
})

test('mask application', () => {
  expect(bin2str(applyMask(
    str2bin('000000000000000000000000000000101010'),
    str2bin('000000000000000000000000000000X1001X')
  )))
    .toEqual('000000000000000000000000000000X1101X')
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(208)
})
