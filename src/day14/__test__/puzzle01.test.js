const { solve, binaryToDecimal, decimalToBinary, applyMask } = require('../puzzle01')

const str2bin = str => str.split('').map(bit => bit === 'X' ? bit : parseInt(bit))

test('binary to decimal', () => {
  expect(binaryToDecimal(str2bin('000000000000000000000000000000000000'))).toEqual(0)
  expect(binaryToDecimal(str2bin('000000000000000000000000000000001011'))).toEqual(11)
  expect(binaryToDecimal(str2bin('000000000000000000000000000001001001'))).toEqual(73)
  expect(binaryToDecimal(str2bin('000000000000000000000000000001100101'))).toEqual(101)
  expect(binaryToDecimal(str2bin('000000000000000000000000000001000000'))).toEqual(64)
})

test('decimal to binary', () => {
  expect(decimalToBinary(0)).toEqual(str2bin('000000000000000000000000000000000000'))
  expect(decimalToBinary(11)).toEqual(str2bin('000000000000000000000000000000001011'))
  expect(decimalToBinary(73)).toEqual(str2bin('000000000000000000000000000001001001'))
  expect(decimalToBinary(101)).toEqual(str2bin('000000000000000000000000000001100101'))
  expect(decimalToBinary(64)).toEqual(str2bin('000000000000000000000000000001000000'))
})

test('apply mask', () => {
  expect(applyMask(str2bin('000000000000000000000000000000001011'),str2bin('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X')))
    .toEqual(str2bin('000000000000000000000000000001001001'))
  expect(applyMask(str2bin('000000000000000000000000000001100101'),str2bin('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X')))
    .toEqual(str2bin('000000000000000000000000000001100101'))
  expect(applyMask(str2bin('000000000000000000000000000000000000'),str2bin('XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X')))
    .toEqual(str2bin('000000000000000000000000000001000000'))
})

test('solves test input', () => {
  const solution = solve(`${__dirname}/input-01.txt`)
  expect(solution).toEqual(165)
})