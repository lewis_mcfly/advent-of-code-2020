const parse = require('./parse')
const { binaryToDecimal, decimalToBinary } = require('./puzzle01')

const applyMask = (binary, mask) => binary.map((bit, index) => mask[index] === 0 ? bit : mask[index])
const matchingBinaries = mask => {
  for (const [index, bit] of mask.entries()) {
    if (bit === 'X') {
      const subAddresses = matchingBinaries(mask.slice(index + 1))
      const zeroSubAddresses = subAddresses.map(submask => [...mask.slice(0, index), 0, ...submask])
      const oneSubAddresses = subAddresses.map(submask => [...mask.slice(0, index), 1, ...submask])
      return [...zeroSubAddresses, ...oneSubAddresses]
    }
  }
  return [mask]
}

const solve = inputPath => {
  const memory = {}
  let mask = Array(36).fill(0)
  const instructions = parse(inputPath)
  for (const instruction of instructions) {
    if (instruction.type === 'mask') {
      mask = instruction.mask
    } else if (instruction.type === 'assign') {
      const maskedAddress = applyMask(decimalToBinary(instruction.address), mask)
      const matchingAddresses = matchingBinaries(maskedAddress)
      for (const address of matchingAddresses) {
        const decimalAddress = binaryToDecimal(address)
        memory[decimalAddress] = instruction.value
      }
    } else {
      throw new Error(`Instruction type ${instruction.type} is invalid`)
    }
  }
  return Object.entries(memory).map(([, value]) => value).reduce((a, b) => a + b)
}

module.exports = { solve, matchingBinaries, applyMask }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


