const parse = require('./parse')


const binaryTable = Array(36).fill(0).map((_, index) => Math.pow(2, index)).reverse()
const applyMask = (binary, mask) => binary.map((bit, index) => mask[index] === 'X' ? bit : mask[index])
const binaryToDecimal = binary => binary.reduce((acc, bit, index) => bit === 0 ? acc : acc + binaryTable[index], 0)
const decimalToBinary = decimal => {
  let remainder = decimal
  const binary = Array(36).fill(0).map((_, index) => {
    if (binaryTable[index] <= remainder) {
      remainder -= binaryTable[index]
      return 1
    }
    return 0
  })
  return binary
}

const solve = inputPath => {
  const memory = {}
  let mask = Array(36).fill(0)
  const instructions = parse(inputPath)
  for (const instruction of instructions) {
    if (instruction.type === 'mask') {
      mask = instruction.mask
    } else if (instruction.type === 'assign') {
      memory[instruction.address] = binaryToDecimal(applyMask(decimalToBinary(instruction.value), mask))
    } else {
      throw new Error(`Instruction type ${instruction.type} is invalid`)
    }
  }
  return Object.entries(memory).map(([, value]) => value).reduce((a, b) => a + b)
}

module.exports = { solve, decimalToBinary, binaryToDecimal, applyMask }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


