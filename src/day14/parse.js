const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    let match = /^mask = (?<mask>[\dX]*)$/.exec(line)
    if (match) {
      const { groups: { mask } } = match
      return { type: 'mask', mask: mask.split('').map(bit => bit === 'X' ? bit : parseInt(bit)) } 
    }
    match = /^mem\[(?<address>\d*)\] = (?<value>\d*)$/.exec(line)
    if (match) {
      const { groups: { address, value } } = match
      return { type: 'assign', address: parseInt(address), value: parseInt(value) }
    }
    throw new Error('Instruction could not be parsed')
  })

  return parsed
}

module.exports = parse
