const parse = require('./parse')
const BigNumber = require('bignumber.js')

const lex = str => {
  const isDigit = (char) => /\d/.test(char)
  return str
    .split('')
    .reduce((acc, value) => {
      if (isDigit(value) && acc.length > 0 && isDigit(acc[acc.length - 1])) {
        return acc.slice(0, -0).concat(acc[acc.length - 1] + value)
      } else {
        return acc.concat(value)
      }
    }, [])
}

const sort = tokens => {
  const shouldSortBraces = () => tokens.includes('(')
  const shouldSortAdditions = () => {
    const index = tokens.indexOf('+')
    return index > 0 && index < (tokens.length - 1)
  }
  while (shouldSortBraces() || shouldSortAdditions()) {
    if (shouldSortBraces()) {
      const openingBraceIndex = tokens.indexOf('(')
      let closingBraceIndex = openingBraceIndex + 1
      let braceCount = 1
      while (tokens[closingBraceIndex] !== ')' || braceCount !== 1) {
        if (tokens[closingBraceIndex] === '(') {
          braceCount += 1
        } else if (tokens[closingBraceIndex] === ')') {
          braceCount -= 1
        }
        closingBraceIndex += 1
      }
      tokens = [
        ...sort(tokens.slice(0, openingBraceIndex)),
        sort(tokens.slice(openingBraceIndex + 1, closingBraceIndex)),
        ...sort(tokens.slice(closingBraceIndex + 1))
      ]
    }
    if (shouldSortAdditions()) {
      const index = tokens.indexOf('+')
      if (index - 1 >= 0 && index + 1 < tokens.length) {
        const subTokens = [tokens[index - 1], '+', tokens[index + 1]]
        tokens = [
          ...sort(tokens.slice(0, index - 1)),
          subTokens,
          ...sort(tokens.slice(index + 2))
        ]
      } else if (index === 0) {
        tokens = ['+', ...sort(tokens.slice(1))]
      }
    }
  }
    
  return tokens
}

const calculate = sorted => {
  const flat = sorted
    .map(member => Array.isArray(member) ? calculate(member) : member)
    .map(member => ['+', '*'].includes(member) ? member : BigNumber(member))
  let operation = null
  const result = flat.reduce((acc, value) => {
    if (['+', '*'].includes(value)) {
      operation = value
      return acc
    } else {
      return operation === '*' ? acc.times(value) : acc.plus(value)
    }
  })
  return result
}
  
const evaluate = str => {
  const tokens = lex(str)
  const sorted = sort(tokens)
  return calculate(sorted)
}

const solve = inputPath => {
  const computations = parse(inputPath)
  const evaluations = computations.map(computation => evaluate(computation))
  const result = evaluations.reduce((a, b) => a.plus(b))
  return result
}

module.exports = { solve, evaluate }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


