const { readlines } = require('../utils')

const parse = inputPath => readlines(inputPath).map(line => line.replace(/\s/g, ''))

module.exports = parse
