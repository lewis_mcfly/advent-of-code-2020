const parse = require('./parse')

const multiplication = 'MULTIPLICATION'
const addition = 'ADDITION'
const constant = 'CONSTANT'

const lex = str => {
  const operation = {
    type: constant
  }
  let itr = str.length - 1

  const readDigit = () => {
    let digit = ''
    while (/\d/.test(str[itr])) {
      digit = str[itr--] + digit
    }
    return parseInt(digit)
  }

  if (str[itr] === ')') {
    let braceCount = 1
    let braceItr = itr - 1
    while (str[braceItr] !== '(' || braceCount !== 1) {
      if (str[braceItr] === ')') {
        braceCount += 1
      } else if (str[braceItr] === '(') {
        braceCount -= 1
      }
      braceItr -= 1
    }

    const openingBraceIndex = braceItr
    operation.right = lex(str.slice(openingBraceIndex + 1, itr))
    itr = openingBraceIndex - 1
  } else {
    operation.right = { type: constant, right: readDigit() }
  }

  if (itr > 0) {
    operation.type = str[itr--] === '+' ? addition : multiplication
    operation.left = lex(str.slice(0, itr + 1))
    return operation
  } else {
    return operation.right
  }
}

const evaluate = str => {
  const lexed = lex(str)

  const calculate = operation => {
    let result = null
    if (operation.type === constant) {
      result = operation.right
    } else if (operation.type === addition) {
      result = calculate(operation.left) + calculate(operation.right)
    } else if (operation.type === multiplication) {
      result = calculate(operation.left) * calculate(operation.right)
    } else {
      throw new Error(`Operation of type "${operation.type}" not handled`)
    }
    return result
  }

  return calculate(lexed)
}

const solve = inputPath => parse(inputPath)
  .map(computation => evaluate(computation))
  .reduce((a, b) => a + b)

module.exports = { solve, evaluate }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


