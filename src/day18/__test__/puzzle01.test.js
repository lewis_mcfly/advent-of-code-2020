const { evaluate } = require('../puzzle01')

test('evaluates correctly 1', () => {
  expect(evaluate('1+2*3+4*5+6')).toEqual(71)
})

test('evaluates correctly 2', () => {
  expect(evaluate('1+(2*3)+(4*(5+6))')).toEqual(51)
})

test('evaluates correctly 3', () => {
  expect(evaluate('2*3+(4*5)')).toEqual(26)
})

test('evaluates correctly 4', () => {
  expect(evaluate('5*9*(7*3*3+9*3+(8+6*4))')).toEqual(12240)})

test('evaluates correctly 5', () => {
  expect(evaluate('((2+4*9)*(6+9*8+6)+6)+2+4*2')).toEqual(13632)
})

test('evaluates correctly 6', () => {
  expect(evaluate('2*(3+(4*5))')).toEqual(46)
})

test('evaluates correctly 7', () => {
  expect(evaluate('(2*3)+(4*5)')).toEqual(26)
})
