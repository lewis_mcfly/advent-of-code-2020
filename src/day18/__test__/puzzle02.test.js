const { evaluate } = require('../puzzle02')

const clean = line => line.replace(/\s/g, '')

test('evaluates correctly 1', () => {
  expect(evaluate('1+2*3+4*5+6').toNumber()).toEqual(231)
})

test('evaluates correctly 2', () => {
  expect(evaluate('1+(2*3)+(4*(5+6))').toNumber()).toEqual(51)
})

test('evaluates correctly 3', () => {
  expect(evaluate('2*3+(4*5)').toNumber()).toEqual(46)
})

test('evaluates correctly 4', () => {
  expect(evaluate('5*9*(7*3*3+9*3+(8+6*4))').toNumber()).toEqual(669060)})

test('evaluates correctly 5', () => {
  expect(evaluate('((2+4*9)*(6+9*8+6)+6)+2+4*2').toNumber()).toEqual(23340)
})

test('evaluates correctly 6', () => {
  expect(evaluate('2*(3+(4*5))').toNumber()).toEqual(46)
})

test('evaluates correctly 7', () => {
  expect(evaluate('(2*3)+(4*5)').toNumber()).toEqual(26)
})

test('evaluates correctly 8', () => {
  expect(evaluate('11+2*3+4*5+6').toNumber()).toEqual(1001)
})

test('evaluates correctly 9', () => {
  expect(evaluate('((2*(3+(4*5))))').toNumber()).toEqual(46)
})

test('evaluates correctly 10', () => {
  expect(evaluate(clean('8 * (5 * 7 + 7) * (4 * 4 + (9 + 3) * 6) + (8 + 8 + 8 * (3 * 3 + 4 * 9 * 6 * 4)) * 9 * 7')).toNumber()).toEqual(3854269440)
})

test('evaluates correctly 11', () => {
  expect(evaluate(clean('8 * 4 * (3 * 2 + 2 + (6 + 9 * 8 + 4 * 5) + (6 + 8 + 4 + 5 + 7 * 3)) + 9 * 6 * ((6 + 4 * 9 * 7 + 7 + 2) * 8 * (3 + 6) + 6 + (3 + 2 + 9 + 5) * 4)')).toString()).toEqual('899723427840')
})

test('evaluates correctly 12', () => {
  expect(evaluate(clean('6 + 8 + 4 + 5 + 7 * 3')).toString()).toEqual('90')
})

test('evaluates correctly 13', () => {
  expect(evaluate(clean('6 + 4 * 9 * 7 + 7 + 2')).toString()).toEqual('1440')
})
