const parse = require('./parse')

const solve = inputPath => {
  const instructions = parse(inputPath)
  const ship = { x: 0, y: 0, direction: 90 }
  
  const turn = degrees => {
    ship.direction = (ship.direction + degrees) % 360
    if (ship.direction < 0) {
      ship.direction = 360 + ship.direction
    }
  }

  for (const instruction of instructions) {
    ({
      N: argument => ship.y += argument,
      S: argument => ship.y -= argument,
      E: argument => ship.x += argument,
      W: argument => ship.x -= argument,
      L: argument => turn(-argument),
      R: argument => turn(argument),
      F: argument => {
        ({
          0: () => ship.y += argument,
          90: () => ship.x += argument,
          180: () => ship.y -= argument,
          270: () => ship.x -= argument
        })[ship.direction]()
      }
    })[instruction.type](instruction.argument)
  }

  return Math.abs(ship.x) + Math.abs(ship.y)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
