const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    let { groups: { type, argument } } = /^(?<type>[NSEWLRF])(?<argument>\d*)$/.exec(line)
    return { type, argument: parseInt(argument) }
  })

  return parsed
}

module.exports = parse
