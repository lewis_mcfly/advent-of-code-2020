const parse = require('./parse')

const solve = inputPath => {
  const instructions = parse(inputPath)
  let ship = { x: 0, y: 0 }
  let waypoint = { x: 10, y: 1 }
  
  const turn = degrees => {
    waypoint = ({
      0: () => ({ x: waypoint.x, y: waypoint.y }),
      90: () => ({ x: waypoint.y, y: -waypoint.x }),
      180: () => ({ x: -waypoint.x, y: -waypoint.y }),
      270: () => ({ x: -waypoint.y, y: waypoint.x }),
    })[degrees]()
  }

  for (const instruction of instructions) {
    ({
      N: argument => waypoint.y += argument,
      S: argument => waypoint.y -= argument,
      E: argument => waypoint.x += argument,
      W: argument => waypoint.x -= argument,
      L: degrees => turn(360 - degrees),
      R: degrees => turn(degrees),
      F: n => {
        ship.x += waypoint.x * n
        ship.y +=  waypoint.y * n
      }
    })[instruction.type](instruction.argument)
  }

  return Math.abs(ship.x) + Math.abs(ship.y)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
