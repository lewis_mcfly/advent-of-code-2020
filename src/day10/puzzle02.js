const parse = require('./parse')

const last = array => array[array.length - 1]

const solve = inputPath => {
  const adapters = parse(inputPath).sort((a, b) => a < b ? -1 : 1)
  adapters.push(last(adapters) + 3)

  return adapters
    .reduce((chunks, adapter) => {
      if (adapter - last(last(chunks)) >= 3) {
        return [...chunks, [adapter]]
      } else {
        last(chunks).push(adapter)
        return chunks 
      }
    }, [[0]])
    .filter(chunk => chunk.length >= 3)
    .reduce((arrangements, chunk) => arrangements * { 3: 2, 4: 4, 5: 7}[chunk.length], 1)
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
