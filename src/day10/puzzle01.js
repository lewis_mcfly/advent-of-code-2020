const parse = require('./parse')

const solve = inputPath => {
  const adapters = [0, ...parse(inputPath).sort((a, b) => a < b ? -1 : 1)]
  adapters.push(adapters[adapters.length - 1] + 3)
  const distibution = {}
  for (let i = 0; i < adapters.length - 1; i += 1) {
    const gap = adapters[i + 1] - adapters[i]
    if (!(gap in distibution)) {
      distibution[gap] = 1
    } else {
      distibution[gap] += 1
    }
  }

  return distibution[1] * distibution[3]
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


