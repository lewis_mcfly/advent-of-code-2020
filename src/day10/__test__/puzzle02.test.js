const solve = require('../puzzle02')

test('solves test input 01', () => {
  const solution = solve(`${__dirname}/input-01.txt`)
  expect(solution).toEqual(8)
})

test('solves test input 02', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(19208)
})
