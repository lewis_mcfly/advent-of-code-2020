const solve = require('../puzzle01')

test('solves test input 01', () => {
  const solution = solve(`${__dirname}/input-01.txt`)
  expect(solution).toEqual(35)
})

test('solves test input 02', () => {
  const solution = solve(`${__dirname}/input-02.txt`)
  expect(solution).toEqual(220)
})
