const { readlines } = require('../utils')

const parse = inputPath => {
  const parsed = readlines(inputPath).map(line => {
    const [type, argument] = line.split(' ')
    return { type, argument: parseInt(argument) }
  })

  return parsed
}

module.exports = parse
