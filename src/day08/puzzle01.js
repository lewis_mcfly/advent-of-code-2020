const parse = require('./parse')

const solve = inputPath => {
  const program = parse(inputPath)

  let iterator = 0
  let accumulator = 0
  let memory = new Array(program.length).fill(false)

  const prototypes = {
    nop: () => { iterator += 1 },
    jmp: argument => { iterator += argument },
    acc: argument => { accumulator += argument; iterator += 1 },
  }

  while (!memory[iterator]) {
    memory[iterator] = true
    const instruction = program[iterator]
    prototypes[instruction.type](instruction.argument)
  }

  return accumulator
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


