const parse = require('./parse')

const InfiniteLoopError = () => {
  this.name = 'InfiniteLoopError'
}
InfiniteLoopError.prototype = Error.prototype

const Instructions = {
  NOPE: 'nop',
  JUMP: 'jmp',
  ACC: 'acc'
}

const execute = program => {
  let iterator = 0
  let accumulator = 0
  let memory = new Array(program.length).fill(false)

  const prototypes = {
    [Instructions.NOPE]: () => { iterator += 1 },
    [Instructions.JUMP]: argument => { iterator += argument },
    [Instructions.ACC]: argument => { accumulator += argument; iterator += 1 },
  }

  while (iterator < program.length) {
    if (memory[iterator]) {
      throw new InfiniteLoopError()
    }
    memory[iterator] = true
    const instruction = program[iterator]
    prototypes[instruction.type](instruction.argument)
  }

  return accumulator
}

const switchType = instruction => ({ 
  ...instruction, 
  type: (instruction.type === Instructions.JUMP ? Instructions.NOPE : Instructions.JUMP), 
})

const solve = inputPath => {
  const program = parse(inputPath)
  for (let i = 0; i < program.length; i += 1) {
    if ([Instructions.JUMP, Instructions.NOPE].includes(program[i].type)) {
      program[i] = switchType(program[i])
      try {
        return execute(program)
      } catch (e) {
        if (!(e instanceof InfiniteLoopError)) {
          throw e
        }
      }
      program[i] = switchType(program[i])
    }
  }
  throw new Error('Did not find the bug')
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}


