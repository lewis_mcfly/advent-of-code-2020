const { calculateNth } = require('./puzzle01')

const solve = (input, debug = false) => calculateNth(input, 30000000, debug)

module.exports = solve

if (require.main === module) {
  const solution = solve('6,19,0,5,7,13,1', true)
  console.log(`The solution is ${solution}`)
}
