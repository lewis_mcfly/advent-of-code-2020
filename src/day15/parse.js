const parse = input => input.split(',').map(n => parseInt(n))

module.exports = parse
