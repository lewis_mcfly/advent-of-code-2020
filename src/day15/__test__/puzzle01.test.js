const { solve } = require('../puzzle01')

test('solves 00', () => {
  expect(solve('0,3,6')).toEqual(436)
})

test('solves 01', () => {
  expect(solve('1,3,2')).toEqual(1)
})

test('solves 02', () => {
  expect(solve('2,1,3')).toEqual(10)
})

test('solves 03', () => {
  expect(solve('1,2,3')).toEqual(27)
})

test('solves 04', () => {
  expect(solve('2,3,1')).toEqual(78)
})

test('solves 05', () => {
  expect(solve('3,2,1')).toEqual(438)
})

test('solves 06', () => {
  expect(solve('3,1,2')).toEqual(1836)
})
