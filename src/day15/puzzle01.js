const parse = require('./parse')

Array.prototype.last = function() {
  return this[this.length - 1]
}

const LAST_TURN = 'lastTurn'
const SECOND_LAST_TURN = 'secondLastTurn'

const calculateNth = (input, n, debug) => {
  const series = parse(input)
  const numbersTurns = series.reduce((acc, value, index) => acc.set(value, (new Map()).set(LAST_TURN, index + 1)), new Map())
  let previousNumber = series.last()
  for (let turn = series.length + 1; turn <= n; turn += 1) {
    if (debug && turn % 100000 === 0) {
      console.log(`Turn ${turn} (${Math.round(turn * 100 / n)}%) - Last number was ${previousNumber}`)
    }
    let numberToSpeak = 0
    if (numbersTurns.has(previousNumber)) {
      if (numbersTurns.get(previousNumber).has(SECOND_LAST_TURN)) {
        numberToSpeak = numbersTurns.get(previousNumber).get(LAST_TURN) - numbersTurns.get(previousNumber).get(SECOND_LAST_TURN)
      }
    }

    if (!numbersTurns.has(numberToSpeak)) {
      numbersTurns.set(numberToSpeak, (new Map()).set(LAST_TURN, turn))
    } else {
      numbersTurns.get(numberToSpeak).set(SECOND_LAST_TURN, numbersTurns.get(numberToSpeak).get(LAST_TURN))
      numbersTurns.get(numberToSpeak).set(LAST_TURN, turn)
    }
    previousNumber = numberToSpeak
  }
  return previousNumber
}

const solve = (input, debug = false) => calculateNth(input, 2020, debug)

module.exports = { calculateNth, solve }

if (require.main === module) {
  const solution = solve('6,19,0,5,7,13,1', true)
  console.log(`The solution is ${solution}`)
}
