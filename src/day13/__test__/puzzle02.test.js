const BigNumber = require('bignumber.js')
const { goldenTimestamp, llcm, chineseRemainder } = require('../puzzle02')

const parse = str => str.split(',').map(bus => bus === 'x' ? bus : parseInt(bus))

test('llcm 01', () => {
  expect(llcm(5, 1, 1).toNumber()).toEqual(1)
  expect(llcm(7, 5, 1).toNumber()).toEqual(3)
  expect(llcm(8, 3, 1).toNumber()).toEqual(3)
})

test('chinese remainder 01', () => {
  expect(chineseRemainder([
    { remainder: BigNumber(3), modulo: BigNumber(5) },
    { remainder: BigNumber(1), modulo: BigNumber(7) },
    { remainder: BigNumber(6), modulo: BigNumber(8) }
  ]).toNumber()).toEqual(78)
})

test('goldenTimestamp 01', () => {
  expect(goldenTimestamp(parse('7,13,x,x,59,x,31,19'))).toEqual(1068781)
})

test('goldenTimestamp 02', () => {
  expect(goldenTimestamp(parse('17,x,13,19'))).toEqual(3417)
})

test('goldenTimestamp 03', () => {
  expect(goldenTimestamp(parse('67,7,59,61'))).toEqual(754018)
})

test('goldenTimestamp 04', () => {
  expect(goldenTimestamp(parse('67,x,7,59,61'))).toEqual(779210)
})

test('goldenTimestamp 05', () => {
  expect(goldenTimestamp(parse('67,7,x,59,61'))).toEqual(1261476)
})

test('goldenTimestamp 06', () => {
  expect(goldenTimestamp(parse('1789,37,47,1889'))).toEqual(1202161486)
})
