const parse = require('./parse')

const solve = inputPath => {
  const { timestamp, buses } = parse(inputPath)
  const nextBus = buses
    .filter(bus => bus  !== 'x')
    .map(bus => ({ line: bus, waitingTime: bus - (timestamp % bus) }))
    .reduce((next, value) => value.waitingTime < next.waitingTime ? value : next)
  return nextBus.waitingTime * nextBus.line
}

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
