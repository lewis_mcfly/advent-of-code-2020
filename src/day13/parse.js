const { readlines } = require('../utils')

const parse = inputPath => {
  const [timestamp, buses] = readlines(inputPath)

  return { timestamp: parseInt(timestamp), buses: buses.split(',').map(bus => bus === 'x' ? bus : parseInt(bus)) }
}

module.exports = parse
