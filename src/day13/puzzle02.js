const BigNumber = require('bignumber.js')
const parse = require('./parse')

const llcm = (a, b, shift) => {
  let n = BigNumber(1)

  while (!n.times(b).modulo(a).isEqualTo(shift)) {
    n = n.plus(1)
  }

  return n
}

const chineseRemainder = values => {
  const N = values.map(value => value.modulo).reduce((a, b) => a.times(b))

  let system = values.map(({ remainder, modulo }) => ({ bi: remainder, ni: modulo, Ni: N.dividedBy(modulo) }))
  system = system.map(({ ni, Ni, bi }) => ({ xi: llcm(ni, Ni.modulo(ni), 1), ni, Ni, bi }))
  system = system.map(({ bi, Ni, xi, ni}) => ({ biNixi: bi.times(Ni).times(xi), bi, Ni, xi, ni }))
  
  const sum = system.reduce((acc, { biNixi }) => acc.plus(biNixi), BigNumber(0))
  return sum.modulo(N)
} 

const goldenTimestamp = buses => {
  const system = buses
    .map((line, index) => ({ line, shift: index }))
    .filter(({ line }) => line !== 'x')
  let maxShift = system.reduce((max, { shift }) => shift > max ? shift : max, 0)
  const bigSystem = system.map(({ line, shift }) => ({ remainder: BigNumber(maxShift - shift), modulo: BigNumber(line) }))
  const solution = chineseRemainder(bigSystem)

  return solution - maxShift
}

const solve = inputPath => goldenTimestamp(parse(inputPath).buses)

module.exports = { solve, goldenTimestamp, chineseRemainder, llcm }

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
