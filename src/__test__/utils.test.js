const { 
  combinations,
  readlines,
  every,
  objectAndArrayFunction,
  find,
  forIn,
  filterObject,
  exists,
  slugify,
  cleanDuplicates,
  sum,
  min,
  max,
  chunkify,
  intersection
} = require('../utils')

test('combinations', () => {
  expect(combinations([1, 2, 3, 4], 0)).toIncludeSameMembers([])
  expect(combinations([1, 2, 3, 4], 1)).toIncludeSameMembers([[1], [2], [3], [4]])
  expect(combinations([1, 2, 3, 4], 2)).toIncludeSameMembers([[1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]])
  expect(combinations([1, 2, 3, 4], 3)).toIncludeSameMembers([[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]])
  expect(combinations([1, 2, 3, 4], 4)).toIncludeSameMembers([[1, 2, 3, 4]])
})

test('readlines', () => {
  expect(readlines(`${__dirname}/__data__/input.txt`)).toEqual(['a b c', 'a', '123'])
})

test('objectAndArrayFunction', () => {
  const commonFunction = objectAndArrayFunction(() => 'object', () => 'array')
  expect(commonFunction({})).toEqual('object')
  expect(commonFunction([])).toEqual('array')

  const commonFunctionWithArgument = objectAndArrayFunction(o => Object.keys(o), a => a.length)
  expect(commonFunctionWithArgument({ foo: 'foo', bar: 'bar' })).toEqual(['foo', 'bar'])
  expect(commonFunctionWithArgument(['foo', 'bar'])).toEqual(2)
})

test('forIn', () => {
  const mockFn = jest.fn()
  forIn({ foo: 'bar', bar: 'baz'}, (key, value) => mockFn(key, value))
  expect(mockFn).toHaveBeenNthCalledWith(1, 'foo', 'bar')
  expect(mockFn).toHaveBeenNthCalledWith(2, 'bar', 'baz')
})

test('every', () => {
  expect(every([1, 2, 3], value => value > 0)).toEqual(true)
  expect(every([1, 2, 3], value => value < 0)).toEqual(false)
  expect(every([1, 2, 3], value => value > 1)).toEqual(false)

  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 0)).toEqual(true)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value < 0)).toEqual(false)
  expect(every({ foo: 1, bar: 2, baz: 3}, value => value > 1)).toEqual(false)
})

test('find', () => {
  expect(find([1, 2, 3], value => value > 0)).toEqual(1)
  expect(find([1, 2, 3], value => value < 0)).toEqual(null)
  expect(find([1, 2, 3], value => value > 1)).toEqual(2)

  expect(find({ foo: 1, bar: 2, baz: 3}, value => value > 0)).toEqual({ foo: 1})
  expect(find({ foo: 1, bar: 2, baz: 3}, value => value < 0)).toEqual(null)
  expect(find({ foo: 1, bar: 2, baz: 3}, value => value > 1)).toEqual({ bar: 2 })
})


test('exists', () => {
  expect(exists([1, 2, 3], value => value > 0)).toEqual(true)
  expect(exists([1, 2, 3], value => value < 0)).toEqual(false)
  expect(exists([1, 2, 3], value => value > 1)).toEqual(true)

  expect(exists({ foo: 1, bar: 2, baz: 3}, value => value > 0)).toEqual(true)
  expect(exists({ foo: 1, bar: 2, baz: 3}, value => value < 0)).toEqual(false)
  expect(exists({ foo: 1, bar: 2, baz: 3}, value => value > 1)).toEqual(true)
})

test('filterObject', () => {
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 0 && key !== 'baz')).toEqual({ foo: 1, bar: 2})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (_, value) => value < 0)).toEqual({})
  expect(filterObject({ foo: 1, bar: 2, baz: 3}, (key, value) => value > 1 && key)).toEqual({ bar: 2, baz: 3})
})

test('slugify', () => {
  expect(slugify('Mdr le slu & gify    ')).toEqual('mdr-le-slu-and-gify')
})

test('cleanDuplicates', () => {
  expect(cleanDuplicates([1, 2, 1, 2, 3])).toEqual([1, 2, 3])
})

test('sum', () => {
  expect(sum([1, 2, 3])).toEqual(6)
})

test('min', () => {
  expect(min([1, 2, 3])).toEqual(1)
})

test('max', () => {
  expect(max([1, 2, 3])).toEqual(3)
})

test('chunkify', () => {
  expect(chunkify([1, null, 3, 4], null)).toEqual([[1], [3, 4]])
  expect(chunkify(['foo', 'bar', '--------', 'baz'], element => element.length > 3)).toEqual([['foo', 'bar'], ['baz']])
})

test('intersection', () => {
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5])).toEqual([1, 2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8])).toEqual([])
  expect(intersection([1, 2, 3, 4])).toEqual([1, 2, 3, 4])
  expect(intersection([1, 2, 3, 4], [1, 2, 4, 5], [2, 3, 4, 5])).toEqual([2, 4])
  expect(intersection([1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12])).toEqual([])
})


