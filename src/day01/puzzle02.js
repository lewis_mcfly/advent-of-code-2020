const { combinations, readlines, find } = require('../utils')

const solve = inputPath => {
  const expenses = readlines(inputPath).map(line => parseInt(line))
  const [a, b, c] = find(combinations(expenses, 3), ([a, b, c]) => a + b + c === 2020)
  return { a, b, c, solution: a * b * c }
}

module.exports = solve

if (require.main === module) {
  const { a, b, c, solution } = solve(`${__dirname}/input.txt`)
  console.log(`a: ${a}, b: ${b}, c: ${c}, a + b + c = ${a+b+c}, a * b * c = ${a*b*c}. Solution is "${solution}"`)
}