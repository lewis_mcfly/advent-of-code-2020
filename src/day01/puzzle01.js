const { combinations, find, readlines } = require('../utils')

const solve = inputPath => {
  const expenses = readlines(inputPath).map(line => parseInt(line))
  const [a, b] = find(combinations(expenses, 2), ([a, b]) => a + b === 2020)
  return { a, b, solution: a * b }
}

module.exports = solve

if (require.main === module) {
  const { a, b, solution } = solve(`${__dirname}/input.txt`)
  console.log(`a: ${a}, b: ${b}, a + b = ${a+b}, a * b = ${a*b}. Solution is "${solution}"`)
}