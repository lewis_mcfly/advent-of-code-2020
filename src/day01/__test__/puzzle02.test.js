const solve = require('../puzzle02')

test('solves test input', () => {
  const { a, b, c, solution } = solve(`${__dirname}/input.txt`)
  expect({ a, b, c }).toContainValues([979, 366, 675])
  expect(solution).toBe(241861950)
})