const solve = require('../puzzle01')

test('solves test input', () => {
  const { a, b, solution } = solve(`${__dirname}/input.txt`)
  expect({ a, b }).toContainValues([1721, 299])
  expect(solution).toBe(514579)
})