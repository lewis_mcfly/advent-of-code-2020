const parse = require('./parse')

const solve = inputPath => parse(inputPath)
  .map(group => group.reduce((acc, answer) => {
    for (const letter of answer) {
      if (!acc.includes(letter)) acc.push(letter)
    }
    return acc
  }, []))
  .map(commonAnswers => commonAnswers.length)
  .reduce((a, b) => a + b)


module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
