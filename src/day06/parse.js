const { readlines } = require('../utils')

const parse = inputPath => {
  const groups = []
  let group = []
  const lines = readlines(inputPath, false)
  for (const line of lines) {
    if (line.length > 0) {
      group.push(line)
    } else {
      groups.push([ ...group])
      group = []
    }
  }

  return groups
}

module.exports = parse
