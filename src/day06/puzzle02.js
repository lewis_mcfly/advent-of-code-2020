const { every } = require('../utils')
const parse = require('./parse')

const solve = inputPath => parse(inputPath)
  .map(answers => {
    const [ firstAnswer, ...otherAnswers ] = answers
    return firstAnswer
      .split('')
      .filter(letter => every(otherAnswers, answer => answer.includes(letter)))
  })
  .map(answers => answers.length)
  .reduce((a, b) => a + b)

module.exports = solve

if (require.main === module) {
  const solution = solve(`${__dirname}/input.txt`)
  console.log(`The solution is ${solution}`)
}
