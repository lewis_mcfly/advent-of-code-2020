[![pipeline status](https://gitlab.com/lewis_mcfly/advent-of-code-2020/badges/master/pipeline.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2020/commits/master)
[![coverage report](https://gitlab.com/lewis_mcfly/advent-of-code-2020/badges/master/coverage.svg)](https://gitlab.com/lewis_mcfly/advent-of-code-2020/commits/master)

# Advent of Code

## 2020

[https://adventofcode.com/2020](https://adventofcode.com/2020)
